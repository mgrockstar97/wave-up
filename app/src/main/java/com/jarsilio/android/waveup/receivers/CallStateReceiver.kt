/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import com.jarsilio.android.waveup.service.ProximitySensorHandler
import timber.log.Timber

class CallStateReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != TelephonyManager.ACTION_PHONE_STATE_CHANGED) {
            Timber.w("Avoid intent spoofing (intent '${intent.action}')")
            return
        }
        val state = intent.extras!!.getString(TelephonyManager.EXTRA_STATE)
        if (state != null) {
            Timber.v("Call state: $state")
            if (state != lastState) { // States seem to show twice (once with number and once without number) I can ignore consecutive states that are equal.
                lastState = state
                if (state == TelephonyManager.EXTRA_STATE_IDLE) {
                    isOngoingCall = false
                    Timber.d("Finished call.")
                } else if (state == TelephonyManager.EXTRA_STATE_OFFHOOK || state == TelephonyManager.EXTRA_STATE_RINGING) {
                    isOngoingCall = true
                    Timber.d("Ongoing call.")
                }
            }
            ProximitySensorHandler.getInstance(context).startOrStopListeningDependingOnConditions()
        }
    }

    companion object {
        var isOngoingCall = false
            private set // Unfortunately I cannot read this live, so I'll just assume there is no ongoing call the first time
        private var lastState = ""
    }
}
